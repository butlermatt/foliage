package main

import (
	"fmt"
	"os"
	"time"

	"github.com/MichaelS11/go-dht"
	"go.etcd.io/bbolt"
)

type valueType byte

const (
	cpuValue valueType = iota
	gpuValue
	temValue
	humValue
)

func (v valueType) String() string {
	switch v {
	case cpuValue:
		return "CPU"
	case gpuValue:
		return "GPU"
	case temValue:
		return "Room"
	case humValue:
		return "Humidity"
	default:
		return ""
	}
}

type value struct {
	t valueType
	v float64
	d time.Time
}

const dbFile = "readings.db"

func main() {
	opts := &bbolt.Options{Timeout: 1 * time.Second}
	db, err := bbolt.Open(dbFile, 0600, opts)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to open database file %s: %v\n", dbFile, err)
		os.Exit(2)
	}

	initdb(db)

	values := make(chan value, 4)

	go cpuTemp(values)
	go gpuTemp(values)

	err = dht.HostInit()
	if err != nil {
		fmt.Fprintf(os.Stderr, "HostInit error: %v\n", err)
		os.Exit(1)
	}

	d, err := dht.NewDHT("GPIO4", dht.Celsius, "")
	if err != nil {
		fmt.Fprintf(os.Stderr, "NewDHT Error: %v\n", err)
		os.Exit(1)
	}

	humidity, temp, err := d.ReadRetry(11)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error reading values: %v\n", err)
		os.Exit(1)
	}

	now := time.Now()

	values <- value{temValue, temp, now}
	values <- value{humValue, humidity, now}

	i := 0
	for a := range values {
		i++
		unit := "°C"
		if a.t == humValue {
			unit = "%"
		}
		fmt.Printf("%d: %v %.2f%s\n", i, a.t, a.v, unit)
		if i == 4 {
			close(values)
		}
	}

	//TODO: Make bboltDB receive the values. Using each valueType as the bucket to store value with a
	// DateTime key (unix time instead of string?)

	//fmt.Printf("Room Temp: %.2f°C\nHumidity:  %.2f%%\n", temp, humidity)
}

func initdb(db *bbolt.DB) {
	db.Update(func(tx *bbolt.Tx) error {
		buckets := []valueType{cpuValue, gpuValue, temValue, humValue}

		for _, bucket := range buckets {
			_, err := tx.CreateBucketIfNotExists([]byte(bucket.String()))
			if err != nil {
				fmt.Fprintf(os.Stderr, "failed to create bucket: %s %v\n", bucket.String(), err)
				os.Exit(2)
			}
		}

		return nil
	})
}
