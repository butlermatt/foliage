module bitbucket.org/butlermatt/foliage

go 1.14

require (
	github.com/MichaelS11/go-dht v0.0.0-20181004212404-be44b9ee7fec
	go.etcd.io/bbolt v1.3.3
	periph.io/x/periph v3.6.2+incompatible // indirect
)
