package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"time"
)

func cpuTemp(c chan<- value) {
	const tempFile = "/sys/class/thermal/thermal_zone0/temp"

	f, err := ioutil.ReadFile(tempFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to read temperature file: %v\n", err)
		return
	}

	f = bytes.TrimSpace(f)

	i, err := strconv.Atoi(string(f))
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to convert temperature \"%s\": %v\n", f, err)
		return
	}

	temp := float64(i) / 1000

	c <- value{cpuValue, temp, time.Now()}
}

func gpuTemp(c chan<- value) {
	out, err := exec.Command("vcgencmd", "measure_temp").Output()
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to execute command: %v\n", err)
		return
	}

	b := bytes.Split(out, []byte("="))
	if len(b) != 2 {
		fmt.Fprintf(os.Stderr, "invalid format string from command output: %q\n", out)
		return
	}

	ind := bytes.IndexRune(b[1], '\'')
	val, err := strconv.ParseFloat(string(b[1][:ind]), 64)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to parse value: %v\n", err)
		return
	}

	c <- value{gpuValue, val, time.Now()}
}
